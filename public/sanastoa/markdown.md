## Sanastoa

---

### Estevaatimukset

Työn vaatimukset jaetaan este- ja haastevaatimuksiin.
Estevaatimukset haittaavat työn sujumista ja lisäävät kuormitusta. Tällaisia ovat esimerkiksi byrokratiaan liittyvät haasteet työssä tai kielteinen työilmapiiri.

---

### Haastevaatimukset

Haastevaatimukset **kuormittavat** mutta myös **motivoivat**, **innostavat** ja **ehkäisevät työssä tylsistymistä**. Tällaisia ovat esimerkiksi työn vastuullisuus, mahdollisuus oppia uutta työssä sekä ajoittainen aikapaine joka saa aikaan innostavaa yhdessä tekemisen meininkiä.
