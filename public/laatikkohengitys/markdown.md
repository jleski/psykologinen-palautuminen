## Laatikkohengitys

![pic](../images/Anxiety-pana.png) <!-- .element height="30%" width="30%" style="margin: 0" -->

---

Hengitä ulos niin, että kaikki ilma tyhjenee keuhkoistasi.
**Pidätä uloshengitystä** ja laske mielessäsi rauhallisesti neljään.

---

Hengitä sisään nenän kautta ja laske samalla neljään.
Pidä keuhkosi täynnä ilmaa ja **pidätä sisäänhengitystä** laskien samalla neljään.

---

Toista harjoitus muutamia kertoja tai niin pitkään kuin se tuntuu **tarpeelliselta**.

---

Laatikkohengitys auttaa tehokkaasti lievittämään stressiä. Sitä opetetaan myös esimerkiksi poliiseille ja sotilaille, jotta he voivat hyödyntää sitä **hyvin kuormittavissa tilanteissa**, joissa on stressistä huolimatta pysyttävä skarppina.
