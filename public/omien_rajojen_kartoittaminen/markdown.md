## Omien rajojen kartoittaminen

![pic](../images/Critical_thinking-cuate.png) <!-- .element height="30%" width="30%" style="margin: 0" -->

---

Varaa harjoitukseen aikaa vähintään **15 minuuttia**. Kirjoita vastauksesi paperille tai puhelimen muistiinpanoihin, jotta voit palata niihin halutessasi myöhemminkin.

---

Mieti **elämääsi**, **käyttäytymistäsi** ja **ihmissuhteitasi**.

---

Halutessasi voit **ulottaa tarkastelun myös työelämään**, sillä vaikka tässä käsittelemme omaehtoisuutta palautumisen osa-alueena, sillä on tärkeä rooli myös työelämässä.

---

Mille asioille olet viimeksi kuluneen kuukauden aikana sanonut **kyllä**, vaikka olisit halunnut sanoa **ei**? Kyse voi olla hyvin pienistä ja arkisista asioista. Listaa kaikki, mitä mieleesi tulee.

---

Pohdi seuraavaksi sitä, mille asioille olet viimeksi kuluneen kuukauden aikana sanonut **ei**, vaikka olisit halunnut sanoa **kyllä**? Kirjoita jälleen ylös kaikki, mitä mieleesi juolahtaa.

---

Tarkastele nyt näitä kahta listaa. **Kummalle listalle** keksit enemmän asioita?

---

Tunnistatko, onko sinun **vaikeampaa** sanoa ei vai kyllä? Liittyvätkö asiat, joissa sinulla on tapana joko joustaa tai rajoittaa itseäsi, tietynlaisiin tilanteisiin tai tiettyihin ihmissuhteisiin?

---

Valitse lopuksi listoiltasi **kolme** useimmin toistuvaa tai sinulle tyypillisimmän tuntuista tilannetta, joissa joustat rajoistasi tai et saa sanottua kyllä asioille, joita **todellisuudessa haluaisit**.

---

Pohdi, **miten haluaisit jatkossa toimia** tällaisissa tilanteissa. Mikä auttaisi sinua pitämään rajoistasi tiukemmin kiinni, tai höllentämään turhan tiukkoja rajoja, joita olet itsellesi asettanut tai sisäistänyt ympäristöstäsi?

---

Tee **konkreettinen** vaihtoehtoinen toimintasuunnitelma tilanteeseen, jonka uskot toistuvan jatkossakin.
