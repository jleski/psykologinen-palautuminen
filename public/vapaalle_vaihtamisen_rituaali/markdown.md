## Vapaalle vaihtamisen rituaali

![pic](../images/Bye-cuate.png) <!-- .element height="30%" width="30%" style="margin: 0" -->

---

Suunnittele itsellesi rituaali, joka auttaa sinua **vaihtamaan vapaalle** niin fyysisesti kuin psyykkisestikin työpäivän päätteeksi.

Älä suhtaudu asiaan **liian kunnianhimoisesti**.

---

Toimiva rutiini on sellainen, jonka pystyt **realistisesti** toteuttamaan ainakin useimpina työpäivinä.

---

Toimiva työpäivänpäätösrituaali ei siis kestä tuntikausia vaan ehkä pikemminkin **korkeintaan puoli tuntia**. Tosin jos sinulla on päivittäin aikaa käydä tunnin lenkillä työpäivän päätyttyä, sekin varmasti toimii loistavasti.

---

Millainen tekeminen auttaisia sinua **jättämään työt työpaikalle** – ainakin kuvaannollisesti, jos työskentelet kotona?

Miten teet itsellesi selväksi, että **työpäivä on päättynyt** ja on aika keskittyä aivan muihin asioihin?

---

Tässä muutamia ideoita, joita voit halutessasi sisällyttää omaan rituaaliisi:

---

1. **Sulje työkoneesi ja –puhelimesi** ja laita ne pois näkyvistä. Jos käytät samoja laitteita myös vapaa-ajalla, sulje ainakin työhön liittyvät ohjelmat tai kirjaudu niistä ulos.

---

2. Kirjoita **to do –lista** seuraavalle työpäivälle.

---

3. Kirjoita **done–lista**, jossa muistutat itseäsi siitä, missä kaikessa olet tänään onnistunut ja mitä olet saanut aikaan.

---

4. Tee **mindfulness- tai hengitysharjoitus**, jossa kohdistat tarkkaavaisuutesi työasioiden sijasta kehoosi ja sen tuntemuksiin. Ehkä myös tunnistat, millaista huomiota kehosi ja mielesi tänään kaipaisivat.

---

5. **Liiku**. Lähde vaikkapa kävelylenkille tai tee pieni kotijumppa – tai miksei kunnon treenikin.

---

6. **Kuuntele** rentouttavaa tai iloa tuottavaa lempimusiikkiasi.

---

7. Vietä hetki **mielenkiintoisen kirjan tai lempisarjasi** parissa.

---

## External 3.3 (Image)

![External Image](https://s3.amazonaws.com/static.slid.es/logo/v2/slides-symbol-512x512.png)

---

## External 3.4 (Math)

`\[ J(\theta_0,\theta_1) = \sum_{i=0} \]`
