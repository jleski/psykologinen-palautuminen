## Mikä arjessasi tukee ja haastaa palautumista?

![pic](../images/Contemplating-pana.png) <!-- .element height="30%" width="30%" style="margin: 0" -->

---

Varaa harjoitukseen aikaa vähintään **15 minuuttia**. Kirjoita vastauksesi ylös, jotta voit palata niihin helposti myöhemmin.

---

Pohdi, millaisia **vaatimuksia** omassa työssäsi on.
Mitkä niistä ovat pelkästään kuormittavia [estevaatimuksia](/sanastoa/#estevaatimukset)?

---

Mitkä puolestaan ovat [haastevaatimuksia](/sanastoa/#haastevaatimukset), jotka voivat kuormittaa, mutta myös **innostaa**? Miten huomaat työn vaatimusten vaikuttavan vapaa-aikaasi ja jaksamiseesi?

---

Pohdi työsi **voimavaroja**. Mikä työssäsi auttaa sinua jaksamaan, motivoitumaan ja tulemaan toimeen vaatimusten kanssa?

---

Tarkastele työsi **vaatimusten ja voimavarojen välistä suhdetta**. Ovatko ne tasapainossa, vai tunnistitko esimerkiksi huomattavasti enemmän vaatimuksia kuin voimavaroja?

---

Työn lisäksi myös **muut arjen rakennuspalikat** vaikuttavat siihen, millaiset mahdollisuudet sinulla on palautua.

---

Millaisia palautumista ja rentoutumista haastavia vaatimuksia sinuun kohdistuu **työsi ulkopuolella?**

---

Esimerkiksi terveyteen, ihmissuhteisiin ja kotitöihin liittyvät vaatimukset tai huolet aiheuttavat monelle stressiä, joka voi **heijastua** myös siihen, miten onnistumme **palautumaan työstressistä**.

---

Entä millaiset työhön liittymättömät tekijät arkielämässäsi **tukevat työstä palautumista?**
