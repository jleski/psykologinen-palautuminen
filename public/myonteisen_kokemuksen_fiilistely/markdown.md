## Myönteisen kokemuksen fiilistely

![pic](../images/Enthusiastic-pana.png) <!-- .element height="30%" width="30%" style="margin: 0" -->

---

Valitse lähipäiviltä tai –viikoilta jokin hetki, kun **onnistuit**.

Se voi myös liittyä työhön, sillä myönteinen työasioiden ajattelu vapaa-ajalla voi edistää hyvinvointia.

---

Voit hyödyntää tätä harjoitusta, kun mietit **työsi merkitystä** ja siihen liittyviä onnistumisia. Keskity myönteisiin asioihin ikävien sijaan.

---

Keskity ajattelemaan onnistumisen **kokemusta** hetkeksi. Palauta mieleesi onnistumisen yksityiskohdat ja se, miltä sinusta silloin tuntui.

---

Havainnoi myös **tämänhetkisiä** tuntemuksiasi: Miltä onnistumisen muistelu ja fiilistely tuntuu kehossasi? Millaisia ajatuksia ja tunteita se herättää?
