## Psykologinen palautuminen

### Harjoituksia

* [Myönteisen kokemuksen fiilistely](myonteisen_kokemuksen_fiilistely/) ![pic](../images/Enthusiastic-pana.png) <!-- .element height="30" width="30" style="margin: 0" -->
* [Vapaalle vaihtamisen rituaali](vapaalle_vaihtamisen_rituaali/) ![pic](../images/Bye-cuate.png) <!-- .element height="30" width="30" style="margin: 0" -->
* [Hengityksen seuranta](hengityksen_seuranta/) ![pic](../images/Meditation-bro.png) <!-- .element height="30" width="30" style="margin: 0" -->
* [Laatikkohengitys](laatikkohengitys/) ![pic](../images/Anxiety-pana.png) <!-- .element height="30" width="30" style="margin: 0" -->
* [Omien rajojen kartoittaminen](omien_rajojen_kartoittaminen/) ![pic](../images/Critical_thinking-cuate.png) <!-- .element height="30" width="30" style="margin: 0" -->
* [Mikä arjessasi tukee ja haastaa palautumista?](palautuminen/) ![pic](../images/Contemplating-pana.png) <!-- .element height="30" width="30" style="margin: 0" -->
