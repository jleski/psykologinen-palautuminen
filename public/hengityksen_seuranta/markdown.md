## Hengityksen seuranta

![pic](../images/Meditation-bro.png) <!-- .element height="30%" width="30%" style="margin: 0" -->

---

Varaa harjoitusta varten **rauhallinen paikka** ja vähintään 2-3 minuuttia aikaa. Voit tehdä harjoituksen joka kerta, kun tunnet sille tarvetta.

---

Ota **mukava asento** ja sulje silmäsi, jos se tuntuu sinusta mukavalta.

---

**Keskitä huomiosi** siihen, miltä hengitys tuntuu eri kehonosissa: sieraimissa, kurkussa tai kaulalla, rintakehällä, pallean ja vatsan alueella, ehkä lantion seudulla tai hartioissa ja selässä asti.

---

Kuuntele hengitystä **sellaisena kuin se juuri nyt on**. Sinun ei tarvitse muuttaa tai säädellä sitä, vaikka niin helposti käykin, kun hengitykseen keskittyy.

---

Kun havaitset huomiosi suuntautuneen pois hengityksestä, **pane se vain merkille** ja tuo tarkkaavaisuutesi lempeästi takaisin hengitykseen.

---

Jatka harjoitusta niin pitkään kuin se **tuntuu tarpeelliselta**.

