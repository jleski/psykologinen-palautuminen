# psykologinen-palautuminen

Tässä Git-repossa on esiteltynä muutamia psykologiseen palautumiseen liittyviä harjoituksia.

Lähteenä olen käyttänyt Anniina Virtasen kirjaa [Psykologinen Palautuminen](https://www.tuumakustannus.fi/Anniina-Virtanen/Psykologinen-palautuminen.html).
